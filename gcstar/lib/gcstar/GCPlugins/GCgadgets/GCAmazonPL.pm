package GCPlugins::GCgadgets::GCAmazonPL;

###################################################
#
#  Copyright 2005-2009 Tian
#  Copyright 2016-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;

use GCPlugins::GCbooks::GCAmazonPL;
use GCPlugins::GCgadgets::GCgadgetsCommon;

{
    package GCPlugins::GCgadgets::GCPluginAmazonPL;

    use base qw(GCPlugins::GCbooks::GCPluginAmazonPL);
    use base qw(GCPlugins::GCgadgets::GCgadgetsPluginsBase);

    sub getEanField
    {
        return 'ean';
    }

    sub getSearchFieldsArray
    {
        my $self = shift;

        GCPlugins::GCgadgets::GCgadgetsPluginsBase::initFieldNames($self);
        $self->{suffix} = 'pl';

        return ['title', 'ean'];
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;
        return 'http://' . $self->baseWWWamazonUrl . "/s?k=$word&__mk_pl_PL=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2FFM0J26RVRST&sprefix=$word%2C100&ref=nb_sb_noss_1";
    }

}

1;
