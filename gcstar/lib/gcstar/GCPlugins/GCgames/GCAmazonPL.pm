package GCPlugins::GCgames::GCAmazonPL;

###################################################
#
#  Copyright 2005-2009 Tian
#  Copyright 2017-2022 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCAmazonPL;
use GCPlugins::GCgames::GCgamesCommon;

{
    package GCPlugins::GCgames::GCPluginAmazonPL;

    use base qw(GCPlugins::GCbooks::GCPluginAmazonPL);
    use base qw(GCPlugins::GCgames::GCgamesPluginsBase);

    sub getEanField
    {
        return 'ean';
    }

    sub getSearchFieldsArray
    {
        my $self = shift;

        GCPlugins::GCgames::GCgamesPluginsBase::initFieldNames($self);
        $self->{suffix} = 'pl';

        return ['name', 'ean'];
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;
        return 'https://www.amazon.' . $self->{suffix} .'/s/ref=nb_sb_noss_1?url=search-alias=videogames&field-keywords=' .$word;
    }
}

1;
