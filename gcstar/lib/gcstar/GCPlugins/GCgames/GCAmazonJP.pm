package GCPlugins::GCgames::GCAmazonJP;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2017-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCAmazon;
use GCPlugins::GCgames::GCgamesCommon;

{
    package GCPlugins::GCgames::GCPluginAmazonJP;

    use base qw(GCPlugins::GCbooks::GCPluginAmazon);
    use base 'GCPlugins::GCgames::GCgamesPluginsBase';

    sub initTranslations
    {
        my $self = shift;

        $self->{translations} = {
            publisher     => "(Publisher|Manufacturer)",
            publication   => "(Release Date|Date First Available|発売日)",
            language      => "Language",
            isbn          => "ISBN",
            dimensions    => "Product Dimensions",
            series        => "Series",
            pages         => "pages",
            by            => "by",
            product       => "(登録情報|商品の説明|Product [Dd]escription|Product information|Product details)",
            brand         => "Brand",
            details       => "Technical Details",
            additional    => "Additional Information",
            end           => "Feedback",
            sponsored     => "Sponsored",
            description   => "Description",
            author        => "Author",
            translator    => "Translator",
            artist        => "Illustrator",
            platform      => "プラットフォーム",
            skip          => "Read more",
            end           => "(Frequently bought|Sponsored products|Customers who|Customer question|Customer reviews|What other items)",
        };
    }

    sub baseWWWamazonUrl
    {
        return "www.amazon.co.jp";    
    }

    sub getName
    {
        return 'Amazon (JP)';
    }
    
    sub getCharset
    {
        my $self = shift;
        return "SHIFT_JIS";
    }

    sub getLang
    {
        return 'JA';
    }

    sub getEanField
    {
        return 'ean';
    }

    sub getSearchFieldsArray
    {
        my $self = shift;

        GCPlugins::GCgames::GCgamesPluginsBase::initFieldNames($self);
        $self->{suffix} = 'co.jp';
        $self->initTranslations;

        return ['name', 'ean'];
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        my $html = $self->loadPage('http://www.amazon.co.jp');

        return 'http://www.amazon.co.jp/s?i=aps&k='.$word.'&ref=nb_sb_noss_1&sprefix='.$word.',aps,606&url=search-alias=aps';
        #return 'http://www.amazon.co.jp/s?i=videogames&k='.$word.'&ref=nb_sb_noss_1&sprefix='.$word.',videogames,606&url=search-alias=videogames';
        #return 'http://www.amazon.co.jp/s?i=videogames&k=uncharted&ref=nb_sb_noss_1&url=search-alias=videogames';
        #return 'http://'. $self->baseWWWamazonUrl.'/s/ref=nb_sb_noss_1?url=search-alias=videogames&field-keywords=' .$word;
    }
}

1;
