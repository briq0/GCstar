package GCPlugins::GCgames::GCAmazonUS;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2017-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCAmazon;
use GCPlugins::GCgames::GCgamesCommon;

{
    package GCPlugins::GCgames::GCPluginAmazonUS;

    use base qw(GCPlugins::GCbooks::GCPluginAmazon);
    use base 'GCPlugins::GCgames::GCgamesPluginsBase';

    sub getName
    {
        return 'Amazon (US)';
    }
    
    sub getLang
    {
        return 'EN';
    }

    sub isPreferred
    {
        return 1;
    }

    sub getSearchFieldsArray
    {
        my $self = shift;

        GCPlugins::GCgames::GCgamesPluginsBase::initFieldNames($self);
        $self->{suffix} = 'com';

        return ['name', 'ean'];
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        my $html = $self->loadPage('http://www.amazon.com');
        return 'http://www.amazon.com/s?k='.$word.'&i=videogames-intl-ship&sprefix='.$word.'%2Cvideogames-intl-ship%2C327&ref=nb_sb_noss_2';
        return 'http://www.amazon.' . $self->{suffix} .'/gp/search/?redirect=true&search-alias=videogames&keywords='.$word;

        return 'http://www.amazon.'.$self->{suffix}.'/s/ref=nb_sb_noss_1?url=search-alias=videogames&field-keywords=' .$word;
    }
}

1;
