package GCPlugins::GCfilms::GCAllocine;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2015-2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCfilms::GCfilmsCommon;

{
    package GCPlugins::GCfilms::GCPluginAllocine;

    use base qw(GCPlugins::GCfilms::GCfilmsPluginsBase);

    use MIME::Base64;

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        if ($self->{parsingList})
        {
            if ($tagname eq 'section')
            {
                $self->{insideResults} = 1;
            }
            if ($self->{insideResults} eq 1)
            {
                if ($tagname eq 'span' && $attr->{class} && $attr->{class} =~ m/meta-title-link/)
                {
                    (my $urlEncoded = $attr->{class}) =~ s/ .*//;
                    my $url = decode_base64("AA".$urlEncoded);
                    $url =~ s/.*fiche/fiche/;
                    $url = "/film/".$url if ($url =~ m/film/);
                    $url = "/series/".$url if ($url =~ m/serie/);
                    if ($self->{pass} eq 1 && $url =~ /^\/(film|series)\/fiche(film|serie)/)
                    {
                        $self->{isInfo} = 1;
                        $self->{itemIdx}++;
                        $self->{itemsList}[ $self->{itemIdx} ]->{url} = $url;
                        if ($self->{searchType} eq 'tvseries')
                        {
                            # use webpage with list of seasons instead of the main page on serie
                            $url =~ s/_gen_cserie=/-/;
                            $url =~ s/.html/\/saisons\//;
                            $self->{itemsList}[ $self->{itemIdx} ]->{url} = undef;
                            $self->{itemsList}[ $self->{itemIdx} ]->{nextUrl} = 'http://www.allocine.fr' . $url;
                        }
                    }
                }
                elsif ($tagname eq 'a' && $attr->{class} =~ m/meta-title-link/)
                {
                    my $url = $attr->{href};
                    return if $url =~ /_gen_cserie/;
                    $self->{isInfo} = 1;
                    $self->{itemIdx}++;
                    $self->{itemsList}[ $self->{itemIdx} ]->{url} = $url;
                }
            }
        }
        else
        {
            if ($tagname eq 'body')
            {
                if ($attr->{id} =~ m/casting/)
                {
                    ($self->{curInfo}->{nextUrl} = $self->{loadedUrl}) =~ s/\/casting.*/.html/;
                    $self->{curInfo}->{actors} = undef;
                    $self->{curInfo}->{nextUrl} =~ s/ficheserie-/ficheserie_gen_cserie=/ if $self->{searchType} eq 'tvseries';
                }
            }
            elsif ($tagname eq 'img' && $attr->{class} && $attr->{class} =~ m/thumbnail-img/ && !$self->{curInfo}->{image})
            {
                my $src = $attr->{src};
                $src = $attr->{'data-src'} if ($src =~ m/base64/);
                $self->{curInfo}->{image} = $src;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ /rating-holder/) # series
            {
                $self->{insidePressRating} = 1;
            }
            elsif ($tagname eq 'h1' && ! $self->{curInfo}->{title})
            {
                $self->{insideTitle} = 1;
            }
            elsif (($tagname eq 'span') && ($attr->{class} && $attr->{class} =~ m/nationality/))
            {
                $self->{insideCountry} = 2;
            }
            elsif (($tagname eq 'span') && ($attr->{class} && $attr->{class} =~ m/stareval-note/) && ($self->{insidePressRating} eq 1))
            {
                $self->{insidePressRating} = 2;
            }
            elsif (($tagname eq 'div') && $attr->{class} && ($attr->{class} eq 'breaker'))
            {
                $self->{insidePressRating} = 0;
            }
            elsif ($tagname eq 'section' && $attr->{id} && ($attr->{id} =~ /synopsis/))
            {
                $self->{insideSynopsis} = 1;
            }
            elsif (($tagname eq 'div') && $attr->{class} && $attr->{class} =~ /synopsis/)
            {
                $self->{insideSynopsis} = 1;
            }
            elsif ($self->{insideSynopsis} eq 1 && $tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/txt/)
            {
                $self->{insideSynopsis} = 2;
            }
            elsif (($tagname eq 'span') && $attr->{class} && $attr->{class} =~ /date/)
            {
                $self->{insideDate} = 2;
            }
            elsif (($tagname eq 'span') && ($self->{insideDate} eq 1))
            {
                $self->{insideDate} = 2;
            }
            elsif ($tagname eq 'a' && $attr->{href} =~ m/\/casting/i && ! $self->{curInfo}->{actors})
            {
                # set URL for casting tab
                $self->{curInfo}->{nextUrl} = 'http://www.allocine.fr'.$attr->{href}
                    if ( (! ($attr->{href} =~ m/episode/)) && (! ($self->{loadedUrl} =~ m/casting/)));
            }
            elsif ($tagname eq 'a' && $attr->{href} =~ m/serie-tv\/genre/i)
            {
                $self->{insideGenre} = 2;
            }
            elsif ($tagname eq 'section' && $attr->{class} =~ /casting-actor/ )
            {
                $self->{insideActor} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/(person-card|md-table-row)/ && $self->{insideActor} > 1)
            {
                $self->{insideActor} = 3;
                $self->addActor if $self->{actor};
                $self->{actor} = '';
                $self->{role} = '';
                $self->{roleEpisodes} = '';
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/meta-body-info/)
            {
                $self->{insideDate} = 2;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/meta-body-nationality/)
            {
                $self->{insideCountry} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/meta-body-item/)
            {
                $self->{insideField} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{class} && $attr->{class} =~ m/episode-card/)
            {
                $self->{insideEpisode} = 1;
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        if ($tagname eq 'div')
        {
            $self->{isInfo} = 0;
            $self->{insideCountry} = 0;
            $self->{insideSynopsis} = 0 if ($self->{insideSynopsis} eq 2);
            $self->{insideDirector} = 0;
            $self->{insideGenre} = 0;
            $self->{insideTime} = 0;
            $self->{insideDate} = 0;
        }
        elsif ($tagname eq 'section')
        {
            $self->addActor if $self->{actor};
            $self->{actor} = '';
            $self->{insideEpisode} = 0;
            $self->{insideResults} = 0;
            $self->{insideActor} = 0;
        }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        $origtext =~ s/[\r\n]//g;
        $origtext =~ s/^\s*//;
        $origtext =~ s/\s*$//;

        return if ($origtext eq '');

        if ($self->{parsingList})
        {
            if ($self->{isInfo} eq 1)
            {
                $self->{itemsList}[ $self->{itemIdx} ]->{title} .= $origtext;
                $self->{isInfo} = 2;
            }
            elsif ($self->{isInfo} eq 2)
            {
                # possible wrong values : Diffusé sur, Date inconnue
                $self->{itemsList}[ $self->{itemIdx} ]->{$self->{publicationField}} = GCUtils::strToTime($origtext,'%d %B %Y',$self->getLang())
                    if ($origtext !~ /(inconnu|diffus)/);
                $self->{isInfo} = 3;
            }
            elsif ($self->{isInfo} eq 3)
            {
                $self->{itemsList}[ $self->{itemIdx} ]->{director} .= $origtext
                     if (! ($origtext =~ /^(De|\|)$/i));
            }
        }
        else
        {
            if ($self->{insideTitle} eq 1)
            {
                $self->{curInfo}->{title} = $origtext if ! $self->{curInfo}->{title};
                $self->{insideTitle} = 0;

                return if ($self->{curInfo}->{actors});
                # two pass plugin : title is set in the first pass
                # loading second web page for casting
                ($self->{curInfo}->{series} = $origtext) =~ s/ : .*//;
                ($self->{curInfo}->{season} = $origtext) =~ s/.*Episodes de la saison //;
            }
            elsif ($self->{insideDirector} eq 2 && $origtext ne ',')
            {
                $self->{curInfo}->{director} .= ', ' if ($self->{curInfo}->{director});
                $self->{curInfo}->{director} .= $origtext;
            }
            elsif ($self->{insideGenre} eq 2 && $origtext ne ',' && $origtext ne '|')
            {
                return if ($origtext =~ m/^,*$/);
                $self->{curInfo}->{genre} .= ',' if ($self->{curInfo}->{genre});
                $self->{curInfo}->{genre} .= $origtext;
            }
            elsif ($self->{insideDate} eq 2)
            {
                $self->{curInfo}->{$self->{publicationField}} = GCUtils::strToTime($origtext,'%d %B %Y',$self->getLang())
                    if (!($origtext =~ /(inconnu|diffus)/) && ! $self->{curInfo}->{$self->{publicationField}});
                $self->{insideDate} = 0;
                $self->{insideTime} = 1;
            }
            elsif ($self->{insideTime} eq 1 && $origtext eq '|')
            {
                $self->{insideTime} = 2;
            }
            elsif ($self->{insideTime} eq 2)
            {
                if ($origtext =~ /(\d+)h\s*(\d+)m.*/)
                {
                    $self->{curInfo}->{time} = ($1*60) + $2;
                }
                elsif ($origtext =~ /(\d+) *min/)
                {
                    $self->{curInfo}->{time} = $1;
                }
                $self->{insideTime} = 0;
                $self->{insideGenre} = 2;
            }
            elsif ($self->{insideCountry} eq 1 && $origtext =~ m/Nationalité/)
            {
                $self->{insideCountry} = 2;
            }
            elsif ($self->{insideCountry} eq 2)
            {
                if ($origtext =~ m/\|/)
                {
                    $self->{insideCountry} = 0;
                }
                $origtext = ',' if $origtext =~ m/^,/;
                $self->{curInfo}->{country} .= $origtext;
            }
            elsif ($self->{insideSerie} && (! $self->{curInfo}->{$self->{publicationField}}))
            {
                if ($origtext =~ m/\(.*\)/)
                {
                    $origtext =~ s/[()]*//g;
                    $self->{curInfo}->{$self->{publicationField}} = $origtext;
                }
            }
            elsif ($self->{insideActor} eq 1 && $origtext =~ m/Acteur/)
            {
                $self->{insideActor} = 2;
            }
            elsif ($self->{insideActor} eq 3)
            {
                return if ($origtext eq ',');
                return if ($origtext =~ m/Voir la liste/i);
                $self->{actor} = $origtext;
                $self->{insideActor} = 4;
            }
            elsif ($self->{insideActor} eq 4)
            {
                if ($origtext =~ m/Episode/)
                {
                    ($self->{roleEpisodes} = $origtext) =~ s/.*(Episode.? ):/(/;
                }
                elsif ($self->{roleEpisodes} && $origtext =~ m/^\d+$/)
                {
                    $self->{roleEpisodes} .= ", "
                        if ($self->{roleEpisodes} && substr($self->{roleEpisodes},-1) ne '(');
                    $self->{roleEpisodes} .= $origtext;
                }
                elsif ($origtext !~ m/-/)
                {
                    $origtext =~ s/\s*plus\s*//;
                    ($self->{role} = $origtext) =~ s/\s*Rôle\s*:\s*//;;
                }
            }
            elsif ($origtext =~ /^Presse$/)
            {
                $self->{insidePressRating} = 1;
            }
            elsif ($self->{insidePressRating} eq 2 && ! $self->{curInfo}->{ratingpress})
            {
                $origtext =~ s/,/./;
                $self->{curInfo}->{ratingpress} .= $origtext * 2;
                $self->{insidePressRating} = 0;
            }
            elsif ($origtext =~ /Date de (reprise|sortie)/)
            {
                $self->{insideDate} = 1;
            }
            elsif ($origtext eq 'Date de sortie inconnue')
            {
                $self->{insideTime} = 1;
            }
            elsif ($origtext =~ m/^Interdit aux moins de (\d+) ans/)
            {
                $self->{curInfo}->{age} = $1;
            }
            elsif ($self->{insideSynopsis} eq 2)
            {
                $self->{curInfo}->{synopsis} .= $origtext.' ';
                # l'espace après un point est parfois oublié par Allociné
                $self->{curInfo}->{synopsis} =~ s/\.([^\s\.])/. $1/g;
            }
            elsif ($self->{insideEpisode} eq 1)
            {
                $self->{curInfo}->{synopsis} .= "\n\n".$origtext;
            }
            elsif ($origtext =~ m/Titre original/)
            {
                $self->{insideOriginal} = 1;
            }
            elsif ($self->{insideOriginal} eq 1)
            {
                $self->{curInfo}->{original} = $origtext;
                $self->{insideOriginal} = 0;
            }
            elsif ($self->{insideField} eq 1)
            {
                $self->{insideDirector} = 2 if ($origtext eq 'De');
                $self->{insideGenre} = 2 if ($origtext =~ m/^Genre/);
                $self->{insideTime} = 1 if ($origtext =~ m/^Dur.*e$/); # durée
                $self->{insideField} = 0;
            }
        }
    }

    sub addActor
    {
        my $self = shift;

        push @{$self->{curInfo}->{actors}}, [$self->{actor}];
        push @{$self->{curInfo}->{actors}->[$self->{actorsCounter}]}, $self->{role};
        $self->{curInfo}->{actors}->[$self->{actorsCounter}]->[1] .= " ".$self->{roleEpisodes}.")"
                if $self->{roleEpisodes};
        $self->{actorsCounter}++;
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();

        $self->{hasField} = {
            title    => 1,
            date     => 1,
            director => 1,
            actors   => 1,
        };

        bless($self, $class);
        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{isInfo}        = 0;
        $self->{isMovie}       = 0;
        $self->{isSerie}       = 0;

        my @tagsName = ('Country', 'Date', 'Director', 'Episode', 'Field', 'Genre', 'Movie',
                        'Original', 'PressRating', 'Results', 'Synopsis', 'Time');
        map { $self->{'inside'.$_} = 0; } @tagsName;

        $self->{curName}       = undef;
        $self->{curUrl}        = undef;
        $self->{actorsCounter} = 0;

        return $html;
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        return "http://www.allocine.fr/rechercher/movie/?q=$word";
    }

    sub getSearchCharset
    {
        my $self = shift;

        # Need urls to be double character encoded
        return 'utf8';
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        return 'http://www.allocine.fr' . $url;
    }

    sub getName
    {
        return 'Allocine.fr';
    }

    sub getAuthor
    {
        return 'Tian - Kerenoc';
    }

    sub getLang
    {
        return 'FR';
    }

    sub getCharset
    {
        # return 'UTF-8'; # For 1.5.0 Win32
        return 'ISO-8859-1'; # For 1.5.0 Win32 with /lib/gcstar/GCPlugins/ ver.1.5.9svn
    }
}

1;
