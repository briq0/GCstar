package GCPlugins::GCbooks::GCbooksBibliotekaNarodowa;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2024 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCbooksCommon;

{
    package GCPlugins::GCbooks::GCPluginBibliotekaNarodowa;

    use base qw(GCPlugins::GCbooks::GCbooksPluginsBase);
    use JSON qw( decode_json );

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{hasField} = {
            title => 1,
            authors => 1,
            publication => 0,
            format => 0,
            edition => 1,
        };

        $self->{languages} = {
            'pol' => 'Polski',
            'eng' => 'Angielski',
            'spa' => 'Hiszpański',
            'fre' => 'Francuski',
            'ger' => 'Niemiecki',
            'rus' => 'Rosyjski',
            'ita' => 'Włoski',
            'por' => 'Portugalski',
        };

        return $self;
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        return $html;
    }

    sub parse
    {
        my ($self, $html) = @_;

        my $json = decode_json($html);
        if ($self->{parsingList})
        {
            print "\nDBG json ".$json->{docs};
            for my $item (@{$json->{docs}})
            {
                print "\nDBG type ".$item->{pnx}->{addata}->{format}[0]." ".$item->{pnx}->{display}->{title}[0];
                   next if ($item->{pnx}->{addata}->{format}[0] ne 'book');
                $self->{itemIdx}++;
                $self->{itemsList}[$self->{itemIdx}]->{url} = $item->{pnx}->{control}->{recordid}[0];
                $self->{itemsList}[$self->{itemIdx}]->{title} = $item->{pnx}->{display}->{title}[0];
                $self->{itemsList}[$self->{itemIdx}]->{authors} = $item->{pnx}->{addata}->{au}[0];
            }
        }
        else
        {
            print "\nDBG json ";
            $self->{curInfo}->{title} = $json->{pnx}->{display}->{title}[0];
            $self->{curInfo}->{publisher} = $json->{pnx}->{display}->{publisher}[0];
            $self->{curInfo}->{description} = $json->{pnx}->{display}->{description}[0];
            $self->{curInfo}->{language} = $json->{pnx}->{display}->{language}[0];
            $self->{curInfo}->{language} = $self->{languages}->{$self->{curInfo}->{language}}
               if ($self->{languages}->{$self->{curInfo}->{language}});
            print "\nDBG language ".$self->{curInfo}->{language};
            $self->{curInfo}->{authors} = $json->{pnx}->{sort}->{author}[0];
            $self->{curInfo}->{authors} =~ s/ *Autor.*//;
            $self->{curInfo}->{authors} =~ s/\(.*//;
            $self->{curInfo}->{format} = $json->{pnx}->{display}->{format}[0];
            $self->{curInfo}->{pages} = $1
               if ($self->{curInfo}->{format} =~ /^(\d+)/);
            $self->{curInfo}->{original} = $json->{pnx}->{display}->{addtitle}[0];
            $self->{curInfo}->{original} =~ s/.*oryginału: *//;
            $self->{curInfo}->{genre} =  $json->{pnx}->{display}->{genre}[0];
            my $isbn = $json->{pnx}->{display}->{identifier}[0];
            $isbn =~ s/.*CISBN\$\$V//;
            $isbn =~ s/\-//g;
            $isbn = $1 if ($isbn =~ /(\d+)/);
            if ($json->{pnx}->{addata}->{seriestitle}[0] =~ /(.*) ; (\d+)/)
            {
                $self->{curInfo}->{serie} = $1;
                $self->{curInfo}->{volume} = $2;
            }
            $self->{curInfo}->{isbn} = $isbn;
            $self->{curInfo}->{cover} = "https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=$isbn/sc.jpg"
                if $isbn;
               my $recordId = $json->{pnx}->{control}->{recordid}[0];
            $self->{curInfo}->{web} = "https://katalogi.bn.org.pl/discovery/fulldisplay?docid=$recordId&context=L&vid=48OMNIS_NLOP:48OMNIS_NLOP&lang=pl&search_scope=NLOP_IZ_NZ&adaptor=Local%20Search%20Engine&tab=LibraryCatalog&offset=0";
        }
    }

    sub getSearchUrl
    {
        my ($self, $word) = @_;

        return "https://katalogi.bn.org.pl/primaws/rest/pub/pnxs?q=any,contains,$word&scope=NLOP_IZ_NZ&acTriggered=false&inst=48OMNIS_NLOP&isCDSearch=false&wspapersActive=false&newspapersSearch=false&offset=0&pcAvailability=true&sort=rank&tab=LibraryCatalog&vid=48OMNIS_NLOP:48OMNIS_NLOP";
        #return "https://katalogi.bn.org.pl/primaws/rest/pub/pnxs?acTriggered=false&blendFacetsSeparately=false&citationTrailFilterByAvailability=true&disableCache=false&getMore=0&inst=48OMNIS_NLOP&isCDSearch=false&lang=pl&limit=10&newspapersActive=false&newspapersSearch=false&offset=0&otbRanking=false&pcAvailability=true&q=any,contains,$word&qExclude=&qInclude=&rapido=false&refEntryActive=false&rtaLinks=true&scope=NLOP_IZ_NZ&searchInFulltextUserSelection=true&skipDelivery=Y&sort=rank&tab=LibraryCatalog&vid=48OMNIS_NLOP:48OMNIS_NLOP";
    }

    sub getItemUrl
    {
        my ($self, $url) = @_;

        return "https://katalogi.bn.org.pl/primaws/rest/pub/pnxs/L/$url?vid=48OMNIS_NLOP:48OMNIS_NLOP&lang=pl&lang=pl"
            if $url;
        #return "https://katalogi.bn.org.pl/primaws/rest/pub/pnxs/L/$url?vid=48OMNIS_NLOP:48OMNIS_NLOP&lang=pl&lang=pl&search_scope=NLOP_IZ_NZ&adaptor=Local%20Search%20Engine"
        return 'http://alpha.bn.org.pl'
    }

    sub getName
    {
        return "Biblioteka Narodowa";
    }

    sub getCharset
    {
        my $self = shift;

        return "ISO-8859-2";
    }

    sub getAuthor
    {
        return 'WG - Kerenoc';
    }

    sub getLang
    {
        return 'PL';
    }

    sub getSearchFieldsArray
    {
        return ['isbn', 'title'];
    }
}

1;
