@echo off

set DIR0=%0
set DIR1=%DIR0:gcstar.bat=..%
set GCSTAR_DIR=%DIR1:"=%

set PATH=%GCSTAR_DIR%\usr\bin;%GCSTAR_DIR%\usr\lib;%PATH%
set GI_TYPELIB_PATH=%GCSTAR_DIR%\usr\lib\typelib
set GSETTINGS_SCHEMA_DIR=%GCSTAR_DIR%\usr\lib\schema
